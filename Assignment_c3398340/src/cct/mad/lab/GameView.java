package cct.mad.lab;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * This class takes care of surface for drawing and touches
 * 
 */

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

	/* Member (state) fields   */
	private GameLoopThread gameLoopThread;
	private Paint paint; //Reference a paint object 
	private Paint paintLarge;
    /** The drawable to use as the background of the animation canvas */
    private Bitmap mBackgroundImage;
    private Sprite sprite; //reference to sprite
    private int numOfSprites = 12; //number of sprites to be generated
    private int hitCount;
    
    //difficulty setting
    public String difficultySetting;
    
    public String scoreToBeat; //HIGHSCORE
    
    /* For the countdown timer */
    private long  startTime;			//Timer to count down from
    private final long interval = 1 * 1000; 	//1 sec interval
    private CountDownTimer countDownTimer; 	//Reference to class
    private boolean timerRunning = false;
    private String displayTime; 		//To display time on the screen

    private boolean gameOver;
    
    private List<Sprite> sprites = new ArrayList<Sprite>();
  
	public GameView(Context context) {
		super(context);
		mBackgroundImage = BitmapFactory.decodeResource(this.getResources(), //reference to background image. 
                R.drawable.starbackground);
		
		// Focus must be on GameView so that events can be handled.
		this.setFocusable(true);
		// For intercepting events on the surface.
		this.getHolder().addCallback(this);
	}
	
	private void createSprites() { //collection of sprites
		
		if (difficultySetting.equals("easy")) {
			numOfSprites = 12;
		}
		if (difficultySetting.equals("medium")) {
			numOfSprites = 18;			
		}
		if (difficultySetting.equals("hard")) {
			numOfSprites = 24;
		}
		
		for (int i=0; i < numOfSprites; i++) {
			sprites.add(createSprite(R.drawable.rocketanimation2));
		}
	}
	
	private Sprite createSprite(int resource) { //collection of sprites
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), resource);
        return new Sprite(this);
	}
	 /* Called immediately after the surface created */
	public void surfaceCreated(SurfaceHolder holder) {
		// We can now safely setup the game start the game loop.
		
		mBackgroundImage = Bitmap.createScaledBitmap(mBackgroundImage, getWidth(), getHeight(), true); //scales the background image to the screen size. 
		
		ResetGame();//Set up a new game up - could be called by a 'play again option'
		gameLoopThread = new GameLoopThread(this.getHolder(), this);
		gameLoopThread.running = true;
		gameLoopThread.start();
		createSprites();
	}
		
	//To initialise/reset game
	private void ResetGame(){
		/* Set paint details */
		gameOver = false;
		sprite = new Sprite(this);
	    paint = new Paint();
		paint.setColor(Color.WHITE); 
		paint.setTextSize(20); 
		hitCount = 0; //initial hitcount is zero.
		//Set timer
		startTime = 20;//Start at 20s to count down
		//Create new object - convert startTime to milliseconds
		countDownTimer=new MyCountDownTimer(startTime*1000,interval);
		countDownTimer.start();//Start it running
		timerRunning = true;
		

	}

	//This class updates and manages the assets prior to drawing - called from the Thread
	public void update(){
		if (gameOver == false) {
			for (Sprite sprite : sprites) {
				sprite.update();
			}
		}
	}
	/**
	 * To draw the game to the screen
	 * This is called from Thread, so synchronisation can be done
	 */
	public void doDraw(Canvas canvas) {
		//Draw all the objects on the canvas
		if (gameOver == false) {
			
		canvas.drawBitmap(mBackgroundImage, 0, 0, null);
		canvas.drawText("The Game ",5,25, paint);
		canvas.drawText("Number of Hits: " + hitCount,5,45, paint);
		canvas.drawText("Time Remaining: " + displayTime,5,65, paint);
		canvas.drawText("Score to Beat: " + scoreToBeat,5,85, paint);
		canvas.drawText("Mode: " + difficultySetting, 5, 105, paint);
		
		for (Sprite sprite : sprites) {
			sprite.draw(canvas);
		}
		}
		
		else if (gameOver == true) {
			canvas.drawBitmap(mBackgroundImage, 0, 0, null);
			paintLarge = new Paint();
			paintLarge.setColor(Color.WHITE);
			paintLarge.setTextSize(40);
			canvas.drawText("Game Over!", 135, 320, paintLarge);
			canvas.drawText("Final Score: " + hitCount, 170, 350, paint);
			canvas.drawText("Return to main menu by using the back key", 60, 375, paint);
		}
	}
	
	public Integer getHitCount() {
		return hitCount;
	}
	
	//To be used if we need to find where screen was touched
	public boolean onTouchEvent(MotionEvent event) {
		for (Sprite sprite : sprites) {
			
			if (sprite.wasItTouched(event.getX(), event.getY())){
				/* For now, just renew the Sprite */
					sprites.remove(sprite); //removes a sprite from the collection of sprites when touched.
					//sprite = new Sprite(this);
			    	   	hitCount++; 
			    	   	break;
			    	}
			}		
		return true;
	}
	
	/* Countdown Timer - private class */
	private class MyCountDownTimer extends CountDownTimer {

	  public MyCountDownTimer(long startTime, long interval) {
			super(startTime, interval);
	  }
	  public void onFinish() {
			displayTime = "Times Over!";
			timerRunning = false;
			gameOver = true;
			countDownTimer.cancel();
	  }
	  public void onTick(long millisUntilFinished) {
			displayTime = " " + millisUntilFinished / 1000;
	  }
	}//End of MyCountDownTimer


	public void surfaceDestroyed(SurfaceHolder holder) {
		gameLoopThread.running = false;
		
		// Shut down the game loop thread cleanly.
		boolean retry = true;
		while(retry) {
			try {
				gameLoopThread.join();
				retry = false;
			} catch (InterruptedException e) {}
		}
	}
	
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		
	}



}
