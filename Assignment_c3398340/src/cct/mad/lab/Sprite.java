package cct.mad.lab;

import java.util.Random;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;


public class Sprite {
    
	//x,y position of sprite - initial position (0,50)
	//private int x = 0; 
	//private int y = 0;
    
    private GameView gameView;
    private Bitmap spritebmp;
    //Width and Height of the Sprite image
    private int bmp_width;
	private int bmp_height;
    // Needed for new random coordinates.
  	private Random random = new Random();
  	private int y = 50 + (random.nextInt(1000)+1);
  	private int x = 50 + (random.nextInt(1000)+1);
  	//random speed
  	private int xSpeed =  random.nextInt(20)-10;//5;//Horizontal increment of position (speed)
    private int ySpeed = random.nextInt(24)-12;//-6;// Vertical increment of position (speed)
   
  	private static final int ANIMATION_ROWS = 2; //sprite animation
  	private static final int ANIMATION_COLUMNS = 3;
  	private int currentFrame = 0;

  	
    
    public Sprite(GameView gameView) {
          this.gameView=gameView;
          spritebmp = BitmapFactory.decodeResource(gameView.getResources(),
                  R.drawable.rocketanimation2);
          this.bmp_width = spritebmp.getWidth() / ANIMATION_COLUMNS;
  		  this.bmp_height= spritebmp.getHeight() / ANIMATION_ROWS;
     }
    //update the position of the sprite
    public void update() {
    	x = x + xSpeed;
     	y = y + ySpeed;
     	currentFrame = ++currentFrame % ANIMATION_COLUMNS;
        wrapAround(); //Adjust motion of sprite.
    }

    public void draw(Canvas canvas) {
    	//Draw sprite image
    	int srcX = currentFrame * bmp_width;
    	int srcY; //row		
    	   if (xSpeed > 0){//Sprite going right; row = 0
    	     		srcY = 0 * bmp_height;
    	   }
    	   else { //Going left; row = 1
    	        	srcY = 1 * bmp_height;
    	   }
    	   
    	Rect src = new Rect(srcX, srcY, srcX + bmp_width, srcY + bmp_height);
    	Rect dst = new Rect(x, y, x + bmp_width, y + bmp_height);

       	canvas.drawBitmap(spritebmp, src , dst, null);
    }
    
  //Code to wrap around
    public void wrapAround(){	
  
    	if (x > gameView.getWidth() - bmp_width - xSpeed || x + xSpeed < 0) {
            xSpeed = -xSpeed;
    	}
    	x = x + xSpeed;
    	if (y > gameView.getHeight() - bmp_height - ySpeed || y + ySpeed < 0) {
    		ySpeed = -ySpeed;
    	}
    	y = y + ySpeed;
    	}
    
    /* Checks if the Sprite was touched. */
    public boolean wasItTouched(float ex, float ey){
    	boolean touched = false; 
    	if ((x <= ex) && (ex < x + bmp_width) &&
    		(y <= ey) && (ey < y + bmp_height)) {
          		touched = true;
    	}
    	return touched;
    }//End of wasItTouched

  
}  
