package cct.mad.lab;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainMenu extends Activity {

	private static final int SCORE_REQUEST_CODE = 1;// The request code for the intent
	public static final String FILE_NAME = "HighscoresFile";
	SharedPreferences settings;
	SharedPreferences.Editor editor;
	
	TextView tvScore;
	TextView tvHighScore;
	String score;
	Intent gameIntent;
	
	//for highscore
	int highScore, lastScore;
	String scoreToBeat = "0";
	
	MediaPlayer mp; //for background music
	
	//difficulty settings
	public boolean easy = true; 
	public boolean medium = false;
	public boolean hard = false;
	
	String difficultySetting;
	 
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		settings = getSharedPreferences(FILE_NAME, 0);
		setContentView(R.layout.game_start);
		tvHighScore = (TextView) findViewById(R.id.tvHighScore);
		tvScore = (TextView) findViewById(R.id.tvGuessGame);
		
		//displaying the saved high score
		int highestScore = settings.getInt("highestScore", 0);
		tvHighScore.setText(Integer.toString(highestScore));
		
		mp = MediaPlayer.create(MainMenu.this, R.raw.appmusic);
		mp.setLooping(true);
		mp.start();
	}
	
	 protected void onPause() {
	        super.onPause();
	        if (this.isFinishing()){
	            mp.stop();
	        }
	    }
	
	public void startGame(View v){
		gameIntent = new Intent(this,GameActivity.class);
		scoreToBeat = tvHighScore.getText().toString();
		gameIntent.putExtra("highscore", scoreToBeat); //highscore
		gameIntent.putExtra("difficultySetting", difficultySetting);
		
		if (easy == true) {
			difficultySetting = "easy";
			gameIntent.putExtra("difficultySetting", difficultySetting);
		}
		
		if (medium == true) {
			difficultySetting = "medium";
			gameIntent.putExtra("difficultySetting", difficultySetting);
		}
		
		if (hard == true) {
			difficultySetting = "hard";
			gameIntent.putExtra("difficultySetting", difficultySetting);
		}
		
	    startActivityForResult(gameIntent, SCORE_REQUEST_CODE );
	  
	}
    /* Create Options Menu */
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	// Respond to item selected on OPTIONS MENU
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		//put data in Intent
		case R.id.easy:
			Toast.makeText(this, "Easy chosen", Toast.LENGTH_SHORT).show();
			easy = true; 
			medium = false;
			hard = false;
			return true;
		case R.id.medium:
			Toast.makeText(this, "Medium chosen", Toast.LENGTH_SHORT).show();
			easy = false;
			medium = true;
			hard = false;
			return true;
		case R.id.hard:
			Toast.makeText(this, "Hard chosen", Toast.LENGTH_SHORT).show();
			easy = false;
			medium = false;
			hard = true;
			return true;
		case R.id.other:
			Toast.makeText(this, "Other chosen", Toast.LENGTH_SHORT).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public void showHighScore(View v) {
		int highestScore = settings.getInt("SavedHighScore", 0);
		Toast.makeText(	MainMenu.this,"Saved Highscore is : " + highestScore,
				Toast.LENGTH_LONG).show();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent retIntent) {
	    // Check which request we're responding to
	    if (requestCode == SCORE_REQUEST_CODE) {
	        // Make sure the request was successful
	        if (resultCode == RESULT_OK) {
	        	if (retIntent.hasExtra("GAME_SCORE")) {
					int scoreFromGame = retIntent.getExtras().getInt("GAME_SCORE");
					tvScore.setText(Integer.toString(scoreFromGame));
					lastScore = Integer.parseInt(tvScore.getText().toString());
					highScore = Integer.parseInt(tvHighScore.getText().toString());
					
					//saving data to preferences file 
					editor = settings.edit();// Create a new editor
					editor.putInt("highscore", highScore); // Storing integer
					editor.commit();// Commit the edits!
					
					
					if (lastScore > highScore) {
						tvHighScore.setText(Integer.toString(scoreFromGame));
						//saving data to preferences file 
						editor = settings.edit();// Create a new editor
						highScore = Integer.parseInt(tvHighScore.getText().toString());
						editor.putInt("highscore", highScore); // Storing integer
						editor.commit();// Commit the edits!
						
					}
	        	}
	        }	
	    }

	}
	
	public void clearScore(View V) {
		editor = settings.edit();// Create a new editor
		editor.clear();
		editor.commit();
		tvHighScore.setText("0");
		Toast.makeText(	MainMenu.this,"Highscore Cleared!",Toast.LENGTH_LONG).show();
	}


}
